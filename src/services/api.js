import axios from 'axios'
import config from '../configfile'

const api = axios.create({
  baseURL: config[process.env.NODE_ENV] ? config[process.env.NODE_ENV].api_url : 'http://localhost:3333'
})

export default api
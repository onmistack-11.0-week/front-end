import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

// Reference:
// Classes - https://rocketseat.com.br/week/aulas/11.0
// Layout  - https://www.figma.com/file/2C2yvw7jsCOGmaNUDftX9n/Be-The-Hero---OmniStack-11?node-id=0%3A1

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
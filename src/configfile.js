export default {
  development: {
    api_url: 'http://localhost:3333/'
  },
  production: {
    api_url: 'https://gdev-be-the-hero.herokuapp.com/'
  }
}
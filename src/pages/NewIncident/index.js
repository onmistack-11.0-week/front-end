import React, {useState} from 'react'
import { Link, useHistory } from 'react-router-dom'
import './styles.css'

import api from '../../services/api'

// Images
import { FiArrowLeft } from 'react-icons/fi' // pacote com vários tipos de icones
//no caso esse 'fi' significa 'feather icons', do site: https://feathericons.com/
import logo from '../../assets/logo.svg'

export default function NewIncident() {

  const [title, setTitle] = useState('')
  const [description, setDescription] = useState('')
  const [value, setValue] = useState('')

  const ongId = localStorage.getItem('ongId')

  const history = useHistory()

  async function handleNewIncident(e) {
    e.preventDefault()

    const data = {
      title,
      description,
      value,
    }

    try {
      const response = await api.post('incidents', data, {
        headers: {
          Authorization: ongId
        }
      })
      alert(`Caso '${response.data.id} - ${title}' cadastrado com sucesso!`)
      history.push('/profile')
    } catch(err) {
      alert('Não foi possível criar um caso, tente novamente.')
    }
  }

  return (
    <div className="new-incident-container">
    <div className="content">
      <section>
        <img src={logo} />

        <h1>Cadastrar novo caso </h1>
        <p>Descreva detalhadamente para encontrar um héroi para resolver isso!</p>
      
        <Link className="back-link" to="/profile">
          <FiArrowLeft size={16} color={'#E02041'} />
          Voltar para o home
        </Link>
      </section>

      <form onSubmit={handleNewIncident}>
        <input placeholder="Título do caso" 
          value={title}
          onChange={e => setTitle(e.target.value)}
        />
        <textarea placeholder="Descrição" 
          value={description}
          onChange={e => setDescription(e.target.value)}
        />
        <input placeholder="Valor em reais" 
          value={value}
          onChange={e =>  setValue(e.target.value)}
        />

        <button className="button" type="submit">Cadastrar</button>
      </form>
    </div>
  </div>
  )
}
import React from 'react';

// Components
import Routes from './routes'

// Styles
import './global.css'

export default function App() {
  return <Routes />
}
